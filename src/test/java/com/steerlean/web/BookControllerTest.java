package com.steerlean.web;

import com.steerlean.persistence.model.Book;
import com.steerlean.persistence.repo.BookRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BookControllerTest {

    @Mock
    private BookRepository mockBookRepository;

    @InjectMocks
    private BookController controller;

    @Test
    public void testFindByTitle() {
        List<Book> books = Mockito.mock(List.class);
        Mockito.when(mockBookRepository.findByTitle("title123")).thenReturn(books);
        List<Book> actual = controller.findByTitle("title123");
        Assert.assertEquals(books, actual);
    }
}